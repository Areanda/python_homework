from datetime import datetime
from threading import Thread
from multiprocessing import Process


text = []
for i in range(10000000):
    text.append(i)
text = str(text)


def write_to_file(data, title):
    with open(f'file_{title}.txt', 'w') as f:
        f.write(data)


if __name__ == '__main__':
    start = datetime.now()
    end = datetime.now()
    print(f'Execution Time in one thread: {end - start}')

    thread1 = Thread(target=write_to_file, kwargs={'data': text, 'title': 'thread1'})
    thread2 = Thread(target=write_to_file, kwargs={'data': text, 'title': 'thread2'})

    start_threads = datetime.now()

    thread1.start()
    thread2.start()
    thread1.join()
    thread2.join()

    end_threads = datetime.now()
    print(f'Execution Time in two threads: {end_threads - start_threads}')

    process1 = Process(target=write_to_file, kwargs={'data': text, 'title': 'process1'})
    process2 = Process(target=write_to_file, kwargs={'data': text, 'title': 'process2'})

    start_processes = datetime.now()

    process1.start()
    process2.start()
    process1.join()
    process2.join()

    end_processes = datetime.now()
    print(f'Execution Time in two processes: {end_processes - start_processes}')

